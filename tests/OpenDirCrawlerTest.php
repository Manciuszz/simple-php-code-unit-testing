<?php
use App\Crawler\OpenDir;

/**
 * Generated by PHPUnit_SkeletonGenerator.
 */
class OpenDirCrawlerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var OpenDir
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new OpenDir('app');

    }

    /**
     * @covers App\Crawler\OpenDir::walk
     */
    public function testWalk()
    {
        $count = 0;

        $this->object->walk(function($file) use (&$count) {

            $this->assertFileExists('app/' . $file);

            $count++;

        });

        $this->assertTrue($count > 0);
    }
}
